import sys
from PyQt5.QtWidgets import *
from PyQt5 import uic
from Connection import Connection

class NewConnection(QMainWindow):
    def __init__(self, parent=None):
        super(NewConnection, self).__init__(parent)
        uic.loadUi("NewConnection.ui", self)
        self.setWindowTitle("New Connection")
        self.parent = parent
        self.btnTest.clicked.connect(self.test)
        self.btnSave.clicked.connect(self.save)
        self.btnCon.clicked.connect(self.connect)

    def test(self):
        user = str(self.txtUser.text())
        pwd = str(self.txtPass.text())
        host = str(self.txtHost.text())
        port = str(self.txtPort.text())
        dbn = str(self.txtDBN.text())
        try:
            con = Connection(user, pwd, host, port, dbn)
            con.connect()
            QMessageBox.information(self, "Success!", "Connection Established.")
            return 1
        except:
            QMessageBox.warning(self, "Error", "Could not connect to Database. Check your credentials")
            return 0

    def save(self):
        if(self.txtCN.text() == ""):
            QMessageBox.warning(self, "Error", "Please provide a Connection Name")
        else:
            if(self.searchInTree(str(self.txtCN.text()))):
                QMessageBox.warning(self, "Error", "Connection already exists")
                return
            if(self.txtHost.text()=="" or self.txtPort.text()=="" or
               self.txtUser.text()=="" or self.txtPass.text()=="" or
               self.txtDBN.text()==""):
                QMessageBox.warning(self, "Error", "Some credentials are missing")
                return
            file = open("connections.sa", "a")
            file.write("T ")
            file.write(self.txtCN.text()+" ")
            file.write(self.txtHost.text()+" ")
            file.write(self.txtPort.text()+" ")
            file.write(self.txtUser.text()+" ")
            file.write(self.txtPass.text()+" ")
            file.write(self.txtDBN.text()+" ")
            file.close()
            treeItem = QTreeWidgetItem([self.txtCN.text()+" "])
            self.parent.treeWidget.topLevelItem(0).addChild(treeItem)
            QMessageBox.information(self, "Save Connection", "Connection Saved.")

    def searchInTree(self, name):
        tree = self.parent.treeWidget.topLevelItem(0);
        children = tree.childCount()

        for x in range(children):
            item = tree.child(x)
            itemName = item.text(0)
            itemName = itemName[:-1]
            if(itemName == name):
                return True
        return False
        

    def connect(self):
        self.searchInTree("local2")
        if(self.test()):
            self.save()
            self.close()
            user = self.txtUser.text()
            pwd = self.txtPass.text()
            host = self.txtHost.text()
            port = self.txtPort.text()
            dbn = self.txtDBN.text()
            self.parent.con.setParams(user, pwd, host, port, dbn)
