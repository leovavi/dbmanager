import sys
from PyQt5.QtWidgets import *
from PyQt5 import uic
from PyQt5.QtGui import QIcon
from NewConnection import NewConnection
from Connection import Connection

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        self.con = Connection("", "", "", "", "")
        uic.loadUi("MainWindow.ui", self)
        self.setWindowTitle("SQL Anywhere Manager")
        self.btnNC.clicked.connect(self.newCon)
        self.treeWidget.setColumnCount(1)
        self.treeWidget.setHeaderLabel("")
        self.treeWidget.header().setFixedHeight(0)
        self.icon = QIcon("C:\\Users\\Leovavi\\Documents\\UNITEC\\Recursos\\plug.png")
        treeItem = QTreeWidgetItem(["Connections"])
        treeItem.setIcon(0, self.icon);
        self.treeWidget.addTopLevelItem(treeItem)
        self.initTree()
        self.treeWidget.itemDoubleClicked.connect(self.doubleClickTWI)
        self.btnRun.clicked.connect(self.executeSql)

    def initTree(self):
        cons = self.readData()
        for x in cons:
            print(x)
            treeItem = QTreeWidgetItem([x])
            treeItem.addChild(QTreeWidgetItem(["Tables"]))
            treeItem.addChild(QTreeWidgetItem(["Indexes"]))
            treeItem.addChild(QTreeWidgetItem(["Procedures"]))
            treeItem.addChild(QTreeWidgetItem(["Triggers"]))
            treeItem.addChild(QTreeWidgetItem(["Views"]))
            treeItem.addChild(QTreeWidgetItem(["Checks"]))
            self.treeWidget.topLevelItem(0).addChild(treeItem)
            
    def doubleClickTWI(item, column_no):
        print(item, column_no)

    def readData(self):
        cons = []
        count = 0
        str = ""
        bool = ""
        with open("connections.sa") as f:
            while True:
                c = f.read(1)
                str += c
                if not c:
                    break
                
                if(c == ' '):
                    count = 1 if count==7 else count+1
                    if(count<=1):
                        bool = str
                    if(count == 2 and bool == "T "):
                        cons.append(str)
                    str = ""
        return cons

    def newCon(self):
        con = NewConnection(self)
        con.show()

    def executeSql(self):
        sql = self.sqlText.toPlainText()
        try:
            curs = self.con.executeSql(sql)
            desc = curs.description
            if(sql.find("select") != -1):
                rowset = curs.fetchall()
                self.result.setRowCount(0)
                self.result.setColumnCount(len(desc))
                labels = []
                pos = 0
                for col in range(len(desc)):
                    labels.append(desc[col][0])

                self.result.setHorizontalHeaderLabels(labels)
                for row in rowset:
                    rowPosition = self.result.rowCount()
                    self.result.insertRow(rowPosition)
                    pos = 0
                    for col in range(len(desc)):
                        self.result.setItem(rowPosition, pos, QTableWidgetItem(str(row[col])))
                        pos = pos+1
        except:
            QMessageBox.warning(self, "Error", "Error while executing SQL")

    def addToTree(self, conName, conn):
        treeItem = QTreeItem([conName+" "])
        treeItem.addChild(QTreeItem(["Tables"]))
        curs = conn.executeSql("Select table_name from systable where table_type = 'BASE' and creator = user_id()")
        
        

def main():
    app = QApplication(sys.argv)
    win = MainWindow()
    win.show()
    app.exec_()

if __name__ == '__main__':
    main()
