import sqlanydb

class Connection:
    def __init__(self, uid, pwd, srv, port, dbn):
        self.uid = uid
        self.pwd = pwd
        self.srv = srv
        self.port = port
        self.dbn = dbn
        self.tableSQL = """
        SELECT
           table_name
        FROM
            systable
        WHERE
            table_type='BASE' AND creator=user_id()"""
        self.indexSQL = """
        select
           index_name
        from
           sysidx i inner join systable t on i.table_id = t.table_id
        where
           t.creator = user_id()"""
        self.procSQL = """
        SELECT 
            proc_name
        FROM
            sysprocedure
        WHERE
            creator = user_id()"""

    def connect(self):
        self.conn = sqlanydb.connect(uid=self.uid, pwd=self.pwd, host=self.srv+":"+self.port, dbn=self.dbn)
        print("after conn")
        self.curs = self.conn.cursor() 

    def disconnect(self):
        self.curs.close()
        self.conn.close()

    def setParams(self, uid, pwd, srv, port, dbn):
        self.uid = uid
        self.pwd = pwd
        self.srv = srv
        self.port = port
        self.dbn = dbn

    def executeSql(self, sql):
        self.connect()
        self.curs.execute(sql)
        return self.curs
        #desc = self.curs.description
        #rowset = self.curs.fetchall()
        #for row in rowset:
        #    for col in range(len(desc)):
        #        print("%s=%s" % (desc[col][0], row[col]))
        #    print()
